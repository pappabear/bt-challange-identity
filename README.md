# BT - Enrollment System #
[BT - Challange (see challenge.md)](./challenge.md)

### Chosen technology stack ###

* Java 8
* Springboot (Web, JPA, Security)
* Thymeleaf
* Swagger-UI
* Lombok
* H2 Database

### About
#### Components
Based on the initial requirements, 3 main components are needed:
    
* ***Enroll Component*** - Responsible for handling client enrollment requests by generating the enrollment documents and handling the adjacent signing process.

* ***Risk Component*** - Given the client ID, it provides the associated risk score.

* ***Clients Component*** - Responsible for checking if the personal information of the client is already stored in the system.

![alternativetext](diagrams/components.png)  

The components have been simulated by splitting the project into modules.  
Each modules JAR has been included into the ***checkhim*** pom.xml which acts as a root. 

#### Flows  

##### 1. Enrollment Flow #####

![alternativetext](diagrams/enroll-diagram.png)  

The administrator submits a new request for a client enrollment.  
The service is processing the request and it provides an enrollment document.  
These documents can have 2 types based on the validity of the user, namely:  

*  Accepted
*  Declined  

The validity of the user is being computed based on 3 factors:  

*  Valid Date (checked internally)
*  Acceptable Risk Score (fetched from the Risk Component)
*  Client's personal information isn't already stored in the system (checked by the Clients Component)

The enrollment documents are first stored in the system with the ***WAITING_FOR_SIGNATURES*** status
until the ***Signing Phase*** is being completed.  
The clients info are first stored in the system with the ***INACTIVE*** status 
until the ***Signing Phase*** is completed for an ***ACCEPTED*** document.

##### 2. Signing Flow #####

![alternativetext](diagrams/sign-diagram.png)  

The administrator submits the signatures of an administrator and of the client.  
If the signatures are valid, the client is enrolled:   
*  Marked as ***ACTIVE*** in the system
*  The corresponding enroll document is marked as ***COMPLETED***   
 

### Setup ###

1. Open the ***app*** file in Intellij IDEA
2. Import the pom.xml-s of the following 3 modules:
    * reputations 
    * clients
    * checkhim
3. Enable Annotation Preprocessor (required by Lombok)
4. Run from Intellij: */checkhim/src/main/java/com/bt/checkhim/CheckhimApplication.java*

### Test Data ###

The system only allows to enroll clients who have an available risk score in the ***risks component***.  
Some sample data is being provided and the following clients have associated risk scores:  

| CNP  |  Reputation     |
|------|-----------------|
|1234567890121	|	22   |
|1234567890122	|	44   |
|1234567890123	|	66   |
|1234567890124	|	88   |
|1234567890125	|	110  |
|1234567890126	|	132  |
|1234567890127	|	154  |
|1234567890128	|	176  |
|1234567890129	|	198  |
|1234567890131	|	22   |
|1234567890132	|	44   |
|1234567890133	|	66   |
|1234567890134	|	88   |
|1234567890135	|	110  |
|1234567890136	|	132  |
|1234567890137	|	154  |
|1234567890138	|	176  |
|1234567890139	|	198  |

### Contact ###

* [Bogdan Iudean](https://www.linkedin.com/in/bogdan-iudean)
