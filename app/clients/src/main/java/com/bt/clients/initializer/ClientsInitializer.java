package com.bt.clients.initializer;

import com.bt.clients.ClientsApplication;
import com.bt.clients.service.ClientsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class ClientsInitializer implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(ClientsApplication.class);

    private ClientsService clientsService;

    @Autowired
    public ClientsInitializer(ClientsService clientsService) {
        this.clientsService = clientsService;

    }

    @Override
    public void run(String... args)
    {
//        log.info("Populating in-memory clients");
//
//        for(int i=1; i < 10; i++) {
//            // lombok didn't work with builder sooo... :(
//            ClientDTO clientDTO = new ClientDTO();
//            clientDTO.setCnp(String.format("123456789012%d", i));
//            clientDTO.setAddress(String.format("str. Baisoara nr %d", i));
//            clientDTO.setFirstName(String.format("X-A-AE-%d", i));
//            clientDTO.setLastName("Musk");
//            clientDTO.setNationality("RO");
//            clientDTO.setIssuedBy("Them");
//            clientDTO.setPlaceOfBirth("Cluj-Napoca");
//            clientDTO.setSeries(String.format("K%d",1));
//            clientDTO.setNumber(String.format("1%d", i));
//            clientDTO.setValidityStart(LocalDate.of(2020, 7, i));
//            clientDTO.setValidityEnd(LocalDate.of(2020, 7, i + 10));
//            clientDTO.setStatus(ClientStatus.ACTIVE);
//
//            clientsService.create(clientDTO);
//
//        }
    }

}
