package com.bt.clients.model;

import com.bt.clients.dto.ClientStatus;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "Client")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ClientEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String cnp;
    private String series;
    private String number;
    private String lastName;
    private String firstName;
    private String nationality;
    private String placeOfBirth;
    private String address;
    private String issuedBy;
    private LocalDate validityStart;
    private LocalDate validityEnd;
    private ClientStatus status;

}
