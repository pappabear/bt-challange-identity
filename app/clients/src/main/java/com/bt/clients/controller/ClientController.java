package com.bt.clients.controller;

import com.bt.clients.dto.ClientDTO;
import javax.persistence.EntityNotFoundException;

import com.bt.clients.service.ClientsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@RequestMapping("/api/client")
@Validated
public class ClientController {

    private ClientsService clientsService;
    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    public ClientController(ClientsService clientsService) {
        this.clientsService = clientsService;
    }


    @Operation(summary = "Check if the client with the given CNP is present in the system")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Client Validation Process - Done",
                    content = {@Content(mediaType = "application/json")})})
    @GetMapping("/isExistent/{cnp}")
    public ResponseEntity<Boolean> isExistent(
                @NotNull(message = "CNP cannot be null")
                @Size(min = 13, max = 13, message = "CNP must have a length of 13 characters")
                @PathVariable String cnp) {
        try {
            clientsService.getByCnp(cnp);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
    }


    @Operation(summary = "Fetch all the clients from the system")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Clients Fetched - Done",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientDTO.class))})})
    @GetMapping("/")
    public ResponseEntity<List<ClientDTO>> getAll() {
        List<ClientDTO> clientDTOs = clientsService.getAll();
        return new ResponseEntity<>(clientDTOs, HttpStatus.OK);

    }
}
