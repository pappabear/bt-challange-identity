package com.bt.clients.service;

import com.bt.clients.dto.ClientDTO;

import javax.persistence.EntityNotFoundException;
import java.util.List;


public interface ClientsService {

    /**
     * Fetch an existing client from the system based on the CNP as an unique identifier
      * @param cnp the unique identifier which provides the selection criteria
     * @return the client if there is a valid client stored under the unique identifier
     * @throws EntityNotFoundException the client corresponding to the identifier couldn't be found
     */
    ClientDTO getByCnp(String cnp) throws EntityNotFoundException;

    /**
     * Fetches all the clients from the system
     * @return all the clients
     */
    List<ClientDTO> getAll();
}
