package com.bt.clients.service.impl;

import com.bt.clients.converter.ClientMapper;
import com.bt.clients.dto.ClientDTO;
import javax.persistence.EntityNotFoundException;
import com.bt.clients.model.ClientEntity;
import com.bt.clients.repository.ClientsRepository;
import com.bt.clients.service.ClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientsServiceImpl implements ClientsService {

    private ClientsRepository clientsRepository;
    private ClientMapper clientMapper;

    @Autowired
    public ClientsServiceImpl(ClientsRepository clientsRepository, ClientMapper clientMapper) {
        this.clientsRepository = clientsRepository;
        this.clientMapper = clientMapper;
    }

    @Override
    public ClientDTO getByCnp(String cnp) throws EntityNotFoundException{

        Optional<ClientEntity> clientEntityOptional = clientsRepository.findByCnp(cnp);
        ClientEntity clientEntity = clientEntityOptional.orElseThrow(EntityNotFoundException::new);

        return clientMapper.toDto(clientEntity);
    }

    @Override
    public List<ClientDTO> getAll() {
        List<ClientDTO> clientDTOs = new ArrayList<>();

        Iterable<ClientEntity> clientEntities = clientsRepository.findAll();
        clientEntities.forEach(e -> clientDTOs.add(clientMapper.toDto(e)));

        return clientDTOs;
    }
}
