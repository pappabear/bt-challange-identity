package com.bt.clients.converter;

import com.bt.clients.dto.ClientDTO;
import com.bt.clients.model.ClientEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ClientMapper extends ModelMapper {

    public ClientDTO toDto(ClientEntity entity) {

        ClientDTO dto = this.map(entity, ClientDTO.class);
        return dto;
    }

    public ClientEntity toEntity(ClientDTO dto) {

        ClientEntity entity= this.map(dto, ClientEntity.class);
        return entity;
    }

}
