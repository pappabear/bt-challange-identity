package com.bt.clients.repository;

import com.bt.clients.model.ClientEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientsRepository extends CrudRepository<ClientEntity, Long> {
    Optional<ClientEntity> findByCnp(String cnp);
}
