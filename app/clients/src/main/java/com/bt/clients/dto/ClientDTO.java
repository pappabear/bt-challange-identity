package com.bt.clients.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ClientDTO {

    @NotNull(message = "CNP cannot be null")
    @Pattern(regexp = "^([0-9]{13})$",
             message = "The CNP must contain 13 digits")
    private String cnp;

    @NotNull(message = "Series cannot be null")
    @Size(min = 6, max = 6, message = "Series must have a length of 6 characters")
    private String series;

    @NotNull(message = "Number cannot be null")
    @Size(min = 2, max = 2, message = "Number must have a length of 2 characters")
    private String number;

    @NotNull(message = "Last Name cannot be null")
    @Size(min = 1, max = 50, message = "Last Name must have a length between 1 - 50 characters")
    private String lastName;

    @NotNull(message = "First Name cannot be null")
    @Size(min = 1, max = 50, message = "First Name must have a length between 1 - 50 characters")
    private String firstName;

    @NotNull(message = "Nationality cannot be null")
    @Size(min = 1, max = 20, message = "Nationality must have a length between 1 - 20 characters")
    private String nationality;

    @NotNull(message = "Place Of Birth cannot be null")
    @Size(min = 1, max = 50, message = "Place of Birth must have a length between 1 - 50 characters")
    private String placeOfBirth;

    @NotNull(message = "Address cannot be null")
    @Size(min = 1, max = 50, message = "Address must have a length between 1 - 50 characters")
    private String address;

    @NotNull(message = "Issuer cannot be null")
    @Size(min = 1, max = 20, message = "Issuer must have a length between 1 - 20 characters")
    private String issuedBy;

    @NotNull(message = "Start date can't be null")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate validityStart;

    @NotNull(message = "Start end can't be null")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate validityEnd;

    private ClientStatus status;

}
