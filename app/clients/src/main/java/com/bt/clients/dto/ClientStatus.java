package com.bt.clients.dto;

public enum ClientStatus {
    ACTIVE,
    INACTIVE
}
