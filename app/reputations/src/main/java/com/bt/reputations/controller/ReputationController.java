package com.bt.reputations.controller;

import com.bt.reputations.dto.CnpReputationDTO;
import com.bt.reputations.service.ReputationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@RequestMapping("/api/reputation")
@Validated
public class ReputationController {

    private ReputationService reputationService;

    @Autowired
    public ReputationController(ReputationService reputationService) {
        this.reputationService = reputationService;
    }

    @Operation(summary = "Fetch the reputation corresponding to the provided CNP")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                         description = "Reputation Fetching - Done",
                         content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                         description = "CNP not found, can't fetch reputation",
                         content = {@Content(mediaType = "application/json")})})
    @GetMapping("/{cnp}")
    public ResponseEntity<?> getFor(
            @NotNull(message = "CNP cannot be null")
            @Size(min = 13, max = 13, message = "CNP must have a length of 13 characters")
            @PathVariable String cnp) {
        try {
            Integer reputation = reputationService.getReputationFor(cnp);
            return new ResponseEntity<>(reputation, HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>("Could not fetch reputation for the provided CNP as the identifier couldn't be found in the system", HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Fetch the reputation of each CNP")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Clients Fetched - Done",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CnpReputationDTO.class))})})
    @GetMapping("/")
    public ResponseEntity<List<CnpReputationDTO>> getAll() {
        List<CnpReputationDTO> reputationDTOs = reputationService.getAll();
        return new ResponseEntity<>(reputationDTOs, HttpStatus.OK);

    }
}
