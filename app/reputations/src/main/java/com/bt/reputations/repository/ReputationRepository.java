package com.bt.reputations.repository;

import com.bt.reputations.model.CnpReputationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReputationRepository extends CrudRepository<CnpReputationEntity, Long> {
    Optional<CnpReputationEntity> findByCnp(String cnp);
}
