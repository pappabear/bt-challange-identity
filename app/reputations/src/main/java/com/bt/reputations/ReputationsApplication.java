package com.bt.reputations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReputationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReputationsApplication.class, args);
	}

}
