package com.bt.reputations.service;

import com.bt.reputations.dto.CnpReputationDTO;

import java.util.List;

public interface ReputationService {
    /**
     * Get the reputation associated to the provided CNP
     * @param cnp the unique identifier that the reputation is fetched for
     * @return the reputation associated to the CNP
     */
    Integer getReputationFor(String cnp);

    /**
     * Fetch all the reputation scores for all the CNPs
     * @return all the reputation scores
     */
    List<CnpReputationDTO> getAll();
}
