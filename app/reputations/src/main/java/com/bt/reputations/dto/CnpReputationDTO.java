package com.bt.reputations.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CnpReputationDTO {

    private String cnp;
    private Integer reputation;


}
