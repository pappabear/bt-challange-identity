package com.bt.reputations.converter;


import com.bt.reputations.dto.CnpReputationDTO;
import com.bt.reputations.model.CnpReputationEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ReputationMapper extends ModelMapper {

    public CnpReputationDTO toDto(CnpReputationEntity entity) {

        CnpReputationDTO dto = this.map(entity, CnpReputationDTO.class);
        return dto;
    }

    public CnpReputationEntity toEntity(CnpReputationDTO dto) {

        CnpReputationEntity entity= this.map(dto, CnpReputationEntity.class);
        return entity;
    }

}
