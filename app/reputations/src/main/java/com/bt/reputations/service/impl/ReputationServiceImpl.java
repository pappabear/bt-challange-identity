package com.bt.reputations.service.impl;

import com.bt.reputations.converter.ReputationMapper;
import com.bt.reputations.dto.CnpReputationDTO;
import com.bt.reputations.model.CnpReputationEntity;
import com.bt.reputations.repository.ReputationRepository;
import com.bt.reputations.service.ReputationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReputationServiceImpl implements ReputationService {

    private ReputationRepository reputationRepository;
    private ReputationMapper reputationMapper;

    @Autowired
    public ReputationServiceImpl(ReputationRepository reputationRepository, ReputationMapper reputationMapper) {
        this.reputationRepository = reputationRepository;
        this.reputationMapper = reputationMapper;
    }

    @Override
    public Integer getReputationFor(String cnp) {
        Optional<CnpReputationEntity> reputationEntityOptional = reputationRepository.findByCnp(cnp);
        CnpReputationEntity reputationEntity = reputationEntityOptional.orElseThrow(EntityNotFoundException::new);

        return reputationEntity.getReputation();
    }

    @Override
    public List<CnpReputationDTO> getAll() {
        List<CnpReputationDTO> clientDTOs = new ArrayList<>();

        Iterable<CnpReputationEntity> clientEntities = reputationRepository.findAll();
        clientEntities.forEach(e -> clientDTOs.add(reputationMapper.toDto(e)));

        return clientDTOs;
    }
}
