package com.bt.reputations.initializer;

import com.bt.reputations.ReputationsApplication;
import com.bt.reputations.model.CnpReputationEntity;
import com.bt.reputations.repository.ReputationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;



@Component
public class ReputationInitializer implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(ReputationsApplication.class);

    private ReputationRepository reputationRepository;

    @Autowired
    public ReputationInitializer(ReputationRepository reputationRepository) {
        this.reputationRepository = reputationRepository;

    }

    @Override
    public void run(String... args)
    {
        log.info("Populating in-memory reputations for clients");

        // for clients existent in the system
        for(int i=1; i < 10; i++) {
            CnpReputationEntity cnpReputationEntity = createReputatationOfDegree(2, i);
            reputationRepository.save(cnpReputationEntity);

        }

        // for trial clients that will be introduced
        for(int i=1; i < 10; i++) {
            CnpReputationEntity cnpReputationEntity = createReputatationOfDegree(3, i);
            reputationRepository.save(cnpReputationEntity);
        }
    }

    private CnpReputationEntity createReputatationOfDegree(int cnpPrefix, int degree) {
        String cnp =   String.format("12345678901%d%d", cnpPrefix, degree);
        int reputation = Integer.parseInt(
                            String.format("%d%d", degree, degree)
                         ) * 2;
        CnpReputationEntity cnpReputationEntity = new CnpReputationEntity();
        cnpReputationEntity.setCnp(cnp);
        cnpReputationEntity.setReputation(reputation);

        return cnpReputationEntity;
    }

}
