package com.bt.checkhim.dto.issue;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class ClientIssueDTO {
    private ClientIssueType issueType;
    private String issue;
}
