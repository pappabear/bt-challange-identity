package com.bt.checkhim.validator;

import java.time.LocalDate;

public interface DateValidator {

    /**
     * Validates the date of the client document
     * @param endDate the dates to be validated
     * @return true if the date is still valid, false otherwise
     */
    Boolean isDataValid(String endDate);

    /**
     * Validates the dates of the client document
     * @param endDate
     * @return true if the date is still valid, false otherwise
     */
    Boolean isDateValid(LocalDate endDate);
}
