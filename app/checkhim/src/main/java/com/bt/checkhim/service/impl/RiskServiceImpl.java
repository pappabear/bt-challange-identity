package com.bt.checkhim.service.impl;

import com.bt.checkhim.dao.ReputationDao;
import com.bt.checkhim.service.risk.RiskLevel;
import com.bt.checkhim.service.risk.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class RiskServiceImpl implements RiskService {

    private ReputationDao reputationDao;

    @Autowired
    public RiskServiceImpl(ReputationDao reputationDao) {
        this.reputationDao = reputationDao;
    }

    @Override
    public RiskLevel getClientRiskLevel(String cnp) throws EntityNotFoundException {
        Integer reputationScore = reputationDao.getClientReputation(cnp);
        RiskLevel riskLevel;

        if(reputationScore < 20) {
            riskLevel = RiskLevel.LOW;
        } else if (reputationScore > 20 && reputationScore < 100) {
            riskLevel = RiskLevel.MEDIUM;
        } else {
            riskLevel = RiskLevel.HIGH;
        }

        return riskLevel;
    }
}
