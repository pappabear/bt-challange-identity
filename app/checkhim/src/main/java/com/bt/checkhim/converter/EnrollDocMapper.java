package com.bt.checkhim.converter;

import com.bt.checkhim.dto.enrolldoc.EnrollDocAcceptDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocDeclinedDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocType;
import com.bt.checkhim.dto.issue.ClientIssueDTO;
import com.bt.checkhim.model.EnrollDocEntity;
import com.bt.clients.model.ClientEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class EnrollDocMapper extends ModelMapper {

    /**
     * Convert an entity to a dto
     * @param entity the entity
     * @return the converted dto
     */
    public EnrollDocDTO toDto(EnrollDocEntity entity) {

        EnrollDocDTO dto;
        if(EnrollDocType.ACCEPTED.equals(entity.getType())) {
            dto = this.map(entity, EnrollDocAcceptDTO.class);
        }
        else {
            EnrollDocDeclinedDTO declinedDto = this.map(entity, EnrollDocDeclinedDTO.class);
            declinedDto.setClientIssues(new ArrayList<ClientIssueDTO>() {{
                                            add(new ClientIssueDTO(null, "Not storing the issues for demo"));
                                        }});
            dto = declinedDto;
        }

        dto.setCnp(entity.getClient().getCnp());

        return dto;

    }

    /**
     * COnvert a dto to an entity
     * @param dto the dto to be converted
     * @param clientEntity the client entity necessary to be set on the resulted entity
     * @return the converted entity
     */
    public EnrollDocEntity toEntity(EnrollDocDTO dto, ClientEntity clientEntity) {
        EnrollDocEntity entity= this.map(dto, EnrollDocEntity.class);
        entity.setClient(clientEntity);
        return entity;
    }
}
