package com.bt.checkhim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;

import java.util.Scanner;

@SpringBootApplication
@ComponentScan(basePackages = {"com.bt.checkhim.*", "com.bt.clients", "com.bt.reputations"})
public class CheckhimApplication {


    public static void main(String[] args) {
        SpringApplication.run(CheckhimApplication.class, args);
        printArt();

    }


    public static void printArt() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n").append("\n");
        sb.append(readFile("art.txt")).append("\n")
          .append("Let's start enrolling: ").append("http://localhost:9090/");

        System.out.println(sb.toString());
    }

    public static String readFile(String fileName) {

        StringBuilder sb = new StringBuilder();

        try {
            File cpr = new ClassPathResource(fileName).getFile();
            Scanner input = new Scanner(cpr);

            while (input.hasNextLine())
            {
                sb.append(input.nextLine()).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
