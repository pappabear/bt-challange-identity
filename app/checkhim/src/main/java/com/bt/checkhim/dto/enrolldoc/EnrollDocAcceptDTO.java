package com.bt.checkhim.dto.enrolldoc;

import lombok.*;

@Getter
@Setter
@ToString
public class EnrollDocAcceptDTO extends EnrollDocDTO {

    public EnrollDocAcceptDTO(EnrollDocStatus status, String uuid, String cnp, Boolean clientSigned, Boolean employeeSigned) {
        super(EnrollDocType.ACCEPTED, status, uuid, cnp, clientSigned, employeeSigned);
    }

    public EnrollDocAcceptDTO() {
        this.setType(EnrollDocType.ACCEPTED);
    }
}
