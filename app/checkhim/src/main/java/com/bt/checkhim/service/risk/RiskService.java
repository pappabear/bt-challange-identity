package com.bt.checkhim.service.risk;

public interface RiskService {
    /**
     * Provides the risk level of a client
     * @param cnp the uniwue identifier of the client in terms of risk
     * @return the risk level of the client
     *
     */
    RiskLevel getClientRiskLevel(String cnp);
}
