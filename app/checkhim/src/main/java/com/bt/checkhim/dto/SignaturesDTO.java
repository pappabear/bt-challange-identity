package com.bt.checkhim.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SignaturesDTO {
    @NotNull(message = "The enroll document identificator must be provided")
    private String docUuid;
    @NotNull(message = "The client signature must be provided")
    private Boolean clientSignature;
    @NotNull(message = "The employee signature must be provided")
    private Boolean employeeSignature;
}
