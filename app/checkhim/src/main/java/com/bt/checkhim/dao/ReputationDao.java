package com.bt.checkhim.dao;

import javax.persistence.EntityNotFoundException;

public interface ReputationDao {
    /**
     * Retrieves the reputation score of the client
     * @param cnp the unique reputation score identifier of the client
     * @return the reputation score
     * @throws EntityNotFoundException the value couldn't be fetched
     */
    Integer getClientReputation(String cnp) throws EntityNotFoundException;
}
