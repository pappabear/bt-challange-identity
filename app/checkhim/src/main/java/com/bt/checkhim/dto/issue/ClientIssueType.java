package com.bt.checkhim.dto.issue;

public enum ClientIssueType {
    RISK_TOO_HIGH,
    ALREADY_EXISTS,
    EXPIRED_DOCUMENT
}
