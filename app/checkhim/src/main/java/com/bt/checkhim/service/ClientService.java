package com.bt.checkhim.service;

import javax.persistence.EntityNotFoundException;

public interface ClientService {

    /**
     * Checks if a client
     * @param cnp the unique reputation score identifier of the client
     * @return true if the client is already present in the system, false otherwise
     * @throws EntityNotFoundException the value couldn't be fetched
     */
    Boolean isClientExistent(String cnp) throws EntityNotFoundException;

}
