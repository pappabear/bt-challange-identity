package com.bt.checkhim.service.impl;

import com.bt.checkhim.converter.EnrollDocMapper;
import com.bt.checkhim.dto.SignaturesDTO;
import com.bt.checkhim.dto.issue.ClientIssueDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocAcceptDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocDeclinedDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocStatus;
import com.bt.checkhim.dto.issue.ClientIssueType;
import com.bt.checkhim.exception.ClientAlreadyEnrollException;
import com.bt.checkhim.exception.signatures.SignaturesException;
import com.bt.checkhim.model.EnrollDocEntity;
import com.bt.checkhim.repository.ClientRepository;
import com.bt.checkhim.repository.EnrollDocRepository;
import com.bt.checkhim.service.CheckhimService;
import com.bt.checkhim.service.EnrollService;
import com.bt.clients.converter.ClientMapper;
import com.bt.clients.dto.ClientDTO;
import com.bt.clients.dto.ClientStatus;
import com.bt.clients.model.ClientEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EnrollServiceImpl implements EnrollService {

    private CheckhimService checkhimService;
    private ClientRepository clientRepository;
    private EnrollDocRepository enrollDocRepository;
    private ClientMapper clientMapper;
    private EnrollDocMapper enrollDocMapper;

    @Autowired
    public EnrollServiceImpl(CheckhimService checkhimService,
                             ClientRepository clientRepository,
                             EnrollDocRepository enrollDocRepository,
                             ClientMapper clientMapper,
                             EnrollDocMapper enrollDocMapper) {
        this.checkhimService = checkhimService;
        this.clientRepository = clientRepository;
        this.enrollDocRepository = enrollDocRepository;
        this.clientMapper = clientMapper;
        this.enrollDocMapper = enrollDocMapper;
    }

    @Override
    public EnrollDocDTO enroll(ClientDTO clientDTO) throws ClientAlreadyEnrollException {

        EnrollDocDTO enrollDoc;

        // find potential issues
        List<ClientIssueDTO> clientIssues = checkhimService.check(clientDTO);

        // generate document differentially based on the issues
        if(clientIssues.size() > 0) {
            // Denied
            enrollDoc = new EnrollDocDeclinedDTO() {{
                               setCnp(clientDTO.getCnp());
                               setClientSigned(false);
                               setEmployeeSigned(false);
                               setClientIssues(clientIssues);
                            }};

        }
        else {
            // Accepted
            enrollDoc = new EnrollDocAcceptDTO() {{
                                setCnp(clientDTO.getCnp());
                                setClientSigned(false);
                                setEmployeeSigned(false);
                            }};
        }

        // generate an UUID for the document
        enrollDoc.setUuid(UUID.randomUUID().toString());

        // the client and doc statuses are pending
        clientDTO.setStatus(ClientStatus.INACTIVE);
        enrollDoc.setStatus(EnrollDocStatus.WAITING_FOR_SIGNATURES);


        // see if among the detected issues there is a client info already stored issue
        boolean isClientInfoStored = clientIssues.stream()
                .anyMatch(i -> i.getIssueType().equals(ClientIssueType.ALREADY_EXISTS));


        // see if the client has ongoing enroll processes
        if(isClientInfoStored) {

            ClientEntity clientEntity = getClientByCnp(clientDTO.getCnp());

            if(hasPendingDocs(clientEntity)) {
                throw new ClientAlreadyEnrollException("The client is already enrolling");
            }

            saveDoc(enrollDoc, clientEntity);

        }
        else {
            ClientEntity clientEntity = saveClient(clientDTO);
            saveDoc(enrollDoc, clientEntity);
        }


        return enrollDoc;
    }


    private ClientEntity getClientByCnp(String cnp) {
        Optional<ClientEntity> clientEntityOptional = clientRepository.findByCnp(cnp);
        return clientEntityOptional.orElseThrow(EntityNotFoundException::new);

    }

    private EnrollDocEntity getDocByUuid(String uuid) {
        Optional<EnrollDocEntity> docEntityOptional = enrollDocRepository.findByUuid(uuid);
        return docEntityOptional.orElseThrow(EntityNotFoundException::new);
    }


    private boolean hasPendingDocs(ClientEntity clientEntity) {
        List<EnrollDocEntity> docEntitys = enrollDocRepository.findByClient(clientEntity);
        return docEntitys.stream().anyMatch(d -> EnrollDocStatus.WAITING_FOR_SIGNATURES.equals(d.getStatus()));
    }


    private ClientEntity saveClient(ClientDTO clientDTO) {
        ClientEntity clientEntity = clientMapper.toEntity(clientDTO);
        return clientRepository.save(clientEntity);
    }


    private EnrollDocEntity saveDoc(EnrollDocDTO enrollDoc, ClientEntity clientEntity) {
        // save the doc in the system
        EnrollDocEntity docEntity = enrollDocMapper.toEntity(enrollDoc, clientEntity);
        return enrollDocRepository.save(docEntity);
    }


    @Override
    public List<EnrollDocDTO> getAllEnrollDocs() {
        List<EnrollDocDTO> enrollDTOs = new ArrayList<>();

        Iterable<EnrollDocEntity> enrollDocEntities = enrollDocRepository.findAll();
        enrollDocEntities.forEach(e -> enrollDTOs.add(enrollDocMapper.toDto(e)));

        return enrollDTOs;
    }

    @Override
    public void signDocument(SignaturesDTO signaturesDTO) throws SignaturesException {
        // signed by both
        if(signaturesDTO.getClientSignature() && signaturesDTO.getEmployeeSignature()) {

            EnrollDocEntity docEntity = getDocByUuid(signaturesDTO.getDocUuid());

            // check if its already signed
            if(EnrollDocStatus.COMPLETED.equals(docEntity.getStatus())) {
                throw new SignaturesException("The document is already signed");
            }

            // mark the doc as completed
            docEntity.setStatus(EnrollDocStatus.COMPLETED);
            enrollDocRepository.save(docEntity);

            // mark client as active
            ClientEntity clientEntity = docEntity.getClient();
            clientEntity.setStatus(ClientStatus.ACTIVE);
            clientRepository.save(clientEntity);
        }
        else {
            if(!signaturesDTO.getEmployeeSignature() && !signaturesDTO.getClientSignature()) {
                throw new SignaturesException("Nor the client, nor the employee have signed");
            }
            else if(!signaturesDTO.getEmployeeSignature()) {
                throw new SignaturesException("The employee hasn't signed");
            }
            else if(!signaturesDTO.getClientSignature()) {
                throw new SignaturesException("The client hasn't signed");
            }
        }
    }
}
