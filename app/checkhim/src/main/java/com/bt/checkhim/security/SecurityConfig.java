package com.bt.checkhim.security;

import com.bt.checkhim.security.roles.USER_ROLES;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${spring.security.user.name}")
    private String defaultAdminUsername;

    @Value("${spring.security.user.password}")
    private String defaultAdminPassword;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser(defaultAdminUsername)
                .password(passwordEncoder()
                        .encode(defaultAdminPassword))
                .roles(USER_ROLES.ADMIN.toString());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        String[] permittedStatic = new String[]{
                "/css/**",
                "/img/**"
        };

        String[] permittedComponents = new String[]{
                "/api/client/**",
                "/api/reputation/**"
        };

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/swagger*").hasAnyRole(USER_ROLES.ADMIN.toString(), USER_ROLES.USER.toString())
                .antMatchers("/api/enroll/*").hasAnyRole(USER_ROLES.ADMIN.toString(), USER_ROLES.USER.toString())
                .antMatchers(permittedStatic).permitAll()
                .antMatchers(permittedComponents).permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .defaultSuccessUrl("/swagger-ui.html")
                .and()
                .logout().permitAll();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
