package com.bt.checkhim.dao.impl;

import com.bt.checkhim.dao.ReputationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;

@Component
public class ReputationDaoImpl implements ReputationDao {

    private final RestTemplate restTemplate;

    // In a perfect world I would have registered another properties file and load them from there
    private static final String COMPONENT_PROTOCOL = "http";
    private static final String COMPONENT_ADDRESS = "localhost";
    private static final String COMPONENT_PORT = "9090";
    private static final String COMPONENT_ENDPOINT = "api/reputation";
    private static final String COMPONENT_URL = String.format("%s://%s:%s/%s",  COMPONENT_PROTOCOL,
            COMPONENT_ADDRESS,
            COMPONENT_PORT,
            COMPONENT_ENDPOINT);


    @Autowired
    public ReputationDaoImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Integer getClientReputation(String cnp) throws EntityNotFoundException {
        String url = String.format("%s/%s",  COMPONENT_URL,
                                                cnp);

        try {
            String responseText = this.restTemplate.getForObject(url, String.class);
            return Integer.parseInt(responseText);

        }
        catch(HttpClientErrorException e) {

            throw new EntityNotFoundException(e.getMessage());
        }
        catch (NumberFormatException | RestClientException e) {
            throw new EntityNotFoundException("Invalid response from the reputation component");
        }
    }

}
