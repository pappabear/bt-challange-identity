package com.bt.checkhim.exception;

public class ClientAlreadyEnrollException extends Exception {
    public ClientAlreadyEnrollException(String message) {
        super(message);
    }
}
