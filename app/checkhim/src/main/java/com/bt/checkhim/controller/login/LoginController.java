package com.bt.checkhim.controller.login;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @Value("${spring.security.user.name}")
    private String defaultAdminUsername;

    @Value("${spring.security.user.password}")
    private String defaultAdminPassword;

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("username", defaultAdminUsername);
        model.addAttribute("password", defaultAdminPassword);

        return "login";
    }
}
