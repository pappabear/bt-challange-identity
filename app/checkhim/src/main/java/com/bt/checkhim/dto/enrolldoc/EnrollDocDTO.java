package com.bt.checkhim.dto.enrolldoc;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public abstract class EnrollDocDTO {
    private EnrollDocType type;
    private EnrollDocStatus status;
    private String uuid;
    private String cnp;
    private Boolean clientSigned;
    private Boolean employeeSigned;
}
