package com.bt.checkhim.service.impl;

import com.bt.checkhim.dto.issue.ClientIssueDTO;
import com.bt.checkhim.dto.issue.ClientIssueType;
import com.bt.checkhim.service.CheckhimService;
import com.bt.checkhim.service.risk.RiskLevel;
import com.bt.checkhim.service.risk.RiskService;
import com.bt.checkhim.validator.DateValidator;
import com.bt.clients.dto.ClientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CheckhimServiceImpl implements CheckhimService {

    private ClientServiceImpl clientService;
    private DateValidator dateValidator;
    private RiskService riskService;

    @Autowired
    public CheckhimServiceImpl(ClientServiceImpl clientService, DateValidator dateValidator, RiskService riskService) {
        this.clientService = clientService;
        this.dateValidator = dateValidator;
        this.riskService = riskService;

    }

    @Override
    public List<ClientIssueDTO> check(ClientDTO clientDTO) {
        List<ClientIssueDTO> clientIssues = new ArrayList<>();

        if(clientService.isClientExistent(clientDTO.getCnp())) {
            String clientExistMessage = "The client info is already present in the system";
            clientIssues.add(new ClientIssueDTO(ClientIssueType.ALREADY_EXISTS, clientExistMessage));
        }

        if(!dateValidator.isDateValid(clientDTO.getValidityEnd())) {
            String dateIssueMessage = String.format("The clients expiration date, %s, is invalid", clientDTO.getValidityEnd());
            clientIssues.add(new ClientIssueDTO(ClientIssueType.EXPIRED_DOCUMENT, dateIssueMessage));
        }

        RiskLevel clientRiskLevel = riskService.getClientRiskLevel(clientDTO.getCnp());
        if(clientRiskLevel.equals(RiskLevel.HIGH)) {
            String highRiskLevelMessage = String.format("The client has a %s risk level which exceeds the allowed limit", clientRiskLevel.toString());
            clientIssues.add(new ClientIssueDTO(ClientIssueType.RISK_TOO_HIGH, highRiskLevelMessage));
        }

        return clientIssues;

    }
}
