package com.bt.checkhim.repository;

import com.bt.checkhim.model.EnrollDocEntity;
import com.bt.clients.model.ClientEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EnrollDocRepository extends CrudRepository<EnrollDocEntity, Long> {

    List<EnrollDocEntity> findByClient(ClientEntity clientEntity);

    Optional<EnrollDocEntity> findByUuid(String uuid);
}
