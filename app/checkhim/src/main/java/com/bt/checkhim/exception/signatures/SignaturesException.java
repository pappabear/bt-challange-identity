package com.bt.checkhim.exception.signatures;

public class SignaturesException extends Exception {
    public SignaturesException(String message) {
        super(message);
    }
}
