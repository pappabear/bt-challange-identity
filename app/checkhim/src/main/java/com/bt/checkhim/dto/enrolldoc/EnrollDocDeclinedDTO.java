package com.bt.checkhim.dto.enrolldoc;

import com.bt.checkhim.dto.issue.ClientIssueDTO;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
public class EnrollDocDeclinedDTO extends EnrollDocDTO {

    private List<ClientIssueDTO> clientIssues;

    public EnrollDocDeclinedDTO(EnrollDocStatus status, String uuid, String cnp, Boolean clientSigned, Boolean employeeSigned, List<ClientIssueDTO> clientIssues) {
        super(EnrollDocType.DECLINED, status,uuid, cnp, clientSigned, employeeSigned);
        this.clientIssues = clientIssues;
    }

    public EnrollDocDeclinedDTO() {
        this.setType(EnrollDocType.DECLINED);
    }
}
