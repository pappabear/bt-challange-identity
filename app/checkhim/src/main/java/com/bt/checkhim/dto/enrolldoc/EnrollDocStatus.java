package com.bt.checkhim.dto.enrolldoc;

public enum EnrollDocStatus {
    WAITING_FOR_SIGNATURES,
    COMPLETED
}
