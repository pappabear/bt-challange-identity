package com.bt.checkhim.service;

import com.bt.checkhim.dto.SignaturesDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocDTO;
import com.bt.checkhim.exception.ClientAlreadyEnrollException;
import com.bt.checkhim.exception.signatures.SignaturesException;
import com.bt.clients.dto.ClientDTO;

import java.util.List;

public interface EnrollService {

    /**
     * Generate an enrollment document for the client with the provided credentials
     * @param clientDTO the client credentials
     * @return the Enrollment document for the client
     * @throws ClientAlreadyEnrollException the client is already enrolled into the system
     */
    EnrollDocDTO enroll(ClientDTO clientDTO) throws ClientAlreadyEnrollException;

    /**
     * Sign the enrollment document
     * @param signaturesDTO the info about the client signatures and the targeted document
     * @throws SignaturesException inconsistent signatures
     */
    void signDocument(SignaturesDTO signaturesDTO) throws SignaturesException;

    /**
     * Get all the enrollment documents
     * @return the enrollment documents
     */
    List<EnrollDocDTO> getAllEnrollDocs();
}
