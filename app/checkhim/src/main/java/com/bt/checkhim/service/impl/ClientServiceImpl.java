package com.bt.checkhim.service.impl;

import com.bt.checkhim.dao.ClientsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class ClientServiceImpl implements com.bt.checkhim.service.ClientService {

    private ClientsDao clientsDao;

    @Autowired
    public ClientServiceImpl(ClientsDao clientsDao) {
        this.clientsDao = clientsDao;
    }

    @Override
    public Boolean isClientExistent(String cnp) throws EntityNotFoundException {
        return clientsDao.isClientExistent(cnp);
    }


}
