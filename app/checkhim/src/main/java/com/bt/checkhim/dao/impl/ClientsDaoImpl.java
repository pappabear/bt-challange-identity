package com.bt.checkhim.dao.impl;

import com.bt.checkhim.dao.ClientsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;

@Component
public class ClientsDaoImpl implements ClientsDao {

    private final RestTemplate restTemplate;

    // In a perfect world I would have registered another properties file and load them from there
    private static final String COMPONENT_PROTOCOL = "http";
    private static final String COMPONENT_ADDRESS = "localhost";
    private static final String COMPONENT_PORT = "9090";
    private static final String COMPONENT_ENDPOINT = "api/client";
    private static final String COMPONENT_URL = String.format("%s://%s:%s/%s",  COMPONENT_PROTOCOL,
            COMPONENT_ADDRESS,
            COMPONENT_PORT,
            COMPONENT_ENDPOINT);

    private static final String COMPONENT_CLIENT_EXISTENT_OPTION = "isExistent";

    @Autowired
    public ClientsDaoImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Boolean isClientExistent(String cnp) throws EntityNotFoundException {
        String url = String.format("%s/%s/%s",   COMPONENT_URL,
                                                    COMPONENT_CLIENT_EXISTENT_OPTION,
                                                    cnp);

        try {
            String responseText = this.restTemplate.getForObject(url, String.class);
            return Boolean.parseBoolean(responseText);

        }
        catch (NumberFormatException e) {
            throw new EntityNotFoundException("Could not parse the response from the reputation component");
        }
        catch (RestClientException e) {
            throw new EntityNotFoundException("Could not connect to the clients component in order to determine if the client is already registered");
        }
    }

}
