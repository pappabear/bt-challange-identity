package com.bt.checkhim.service.risk;

public enum RiskLevel {
    LOW,
    MEDIUM,
    HIGH
}
