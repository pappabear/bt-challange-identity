package com.bt.checkhim.service;

import com.bt.checkhim.dto.issue.ClientIssueDTO;
import com.bt.clients.dto.ClientDTO;

import java.util.List;

public interface CheckhimService {
    /**
     * Check if a client is eligible for enrollment
     * @param clientDTO the client to be checked
     * @return the list of potential issues that the client may be prone to
     */
    public List<ClientIssueDTO> check(ClientDTO clientDTO);
}
