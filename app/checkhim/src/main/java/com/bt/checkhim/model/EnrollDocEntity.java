package com.bt.checkhim.model;

import com.bt.checkhim.dto.enrolldoc.EnrollDocStatus;
import com.bt.checkhim.dto.enrolldoc.EnrollDocType;
import com.bt.clients.model.ClientEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class EnrollDocEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private EnrollDocType type;
    private EnrollDocStatus status;
    private String uuid;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    private ClientEntity client;

    private Boolean clientSigned;
    private Boolean employeeSigned;
}
