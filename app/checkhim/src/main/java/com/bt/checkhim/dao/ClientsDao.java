package com.bt.checkhim.dao;

import javax.persistence.EntityNotFoundException;

public interface ClientsDao {
    /**
     * Checks if a client
     * @param cnp the unique reputation score identifier of the client
     * @return true if the already client exists, false otherwise
     * @throws EntityNotFoundException the value couldn't be fetched
     */
    Boolean isClientExistent(String cnp) throws EntityNotFoundException;
}
