package com.bt.checkhim.controller.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

// TODO maybe remove after implementation

@Controller
@RequestMapping("/")
public class RedirectController {

    @GetMapping("/")
    public RedirectView redirectHome() {
        return new RedirectView("swagger-ui.html");
    }
}
