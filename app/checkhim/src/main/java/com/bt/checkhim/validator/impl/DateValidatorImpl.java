package com.bt.checkhim.validator.impl;

import com.bt.checkhim.validator.DateValidator;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DateValidatorImpl implements DateValidator {

    public DateValidatorImpl() {

    }


    @Override
    public Boolean isDataValid(String endDate) {
        try {
            LocalDate endDateD = LocalDate.parse(endDate);
            return isDateValid(endDateD);

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Boolean isDateValid(LocalDate endDate) {
        if (endDate == null) {
            return false;
        }

        LocalDate currentDate = LocalDate.now();
        return endDate.isAfter(currentDate);
    }
}
