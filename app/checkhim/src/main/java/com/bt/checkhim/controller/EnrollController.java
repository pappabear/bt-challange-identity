package com.bt.checkhim.controller;

import com.bt.checkhim.dto.SignaturesDTO;
import com.bt.checkhim.dto.enrolldoc.EnrollDocDTO;
import com.bt.checkhim.exception.ClientAlreadyEnrollException;
import com.bt.checkhim.exception.signatures.SignaturesException;
import com.bt.checkhim.service.EnrollService;
import com.bt.clients.dto.ClientDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;


@RestController
@Validated
@RequestMapping("/api/enroll")
public class EnrollController {

    private EnrollService enrollService;

    @Autowired
    public EnrollController(EnrollService enrollService) {
        this.enrollService = enrollService;
    }

    @Operation(summary = "Enroll a new client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Client Info Saving Process - Done",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Invalid Client Data Provided",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "403",
                    description = "There is already an enrolling process for the client",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "500",
                    description = "Connection problems with the third party systems",
                    content = {@Content(mediaType = "application/json")})

    })
    @PostMapping("/")
    public ResponseEntity<?> enrollClient(@Valid @RequestBody ClientDTO client, Errors errors) {

        try {
            EnrollDocDTO enrollDoc = enrollService.enroll(client);
            return new ResponseEntity<>(enrollDoc, HttpStatus.OK);
        }
        catch (ClientAlreadyEnrollException e) {
            return new ResponseEntity<>("The client already has a pending contract, please sign first", HttpStatus.FORBIDDEN);
        }

    }

    @Operation(summary = "Sign an enrollment document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Signing Process - Done",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",
                    description = "Invalid signatures data provided",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500",
                    description = "Signature problem",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Can't find one of the required entities",
                    content = {@Content(mediaType = "application/json")})

    })
    @PostMapping("/sign")
    public ResponseEntity<?> sign(@Valid @RequestBody SignaturesDTO signatures, Errors errors) {

        try {
            enrollService.signDocument(signatures);
            return new ResponseEntity<>("The client has been successfully enrolled", HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>("The document couldn't be signed as the document or the client info are not available", HttpStatus.NOT_FOUND);
        }
        catch (SignaturesException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Operation(summary = "Fetch all the enroll documents from the system")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Clients Fetched - Done",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = EnrollDocDTO.class))})})
    @GetMapping("/")
    public ResponseEntity<List<EnrollDocDTO>> getAll() {
        List<EnrollDocDTO> enrollDocDTOs = enrollService.getAllEnrollDocs();
        return new ResponseEntity<>(enrollDocDTOs, HttpStatus.OK);

    }


}
