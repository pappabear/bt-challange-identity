# The Challenge

Design, document and partial implement an API that would service a front-end application which enrolls new clients.

## Description

Suppose there is a frontend application whose purpose is to enroll new clients into the core banking system. The Frontdesk Employee uses the application
to input clients data (identity document information) then submit a full Client check.
Based on the checking response, the Frontdesk Employee will 
either generate the enrolment document 
or 
a denial document specifying the reason the enrolment was denied. 
In both cases the generated document will have to be signed by both Client
and Frontdesk Employee and submitted back to system.

## Your tasks

1. Design (create endpoint contracts) the API that will support the above operations (full client check, generating enrolment/denial documents, etc),
create the needed documentation for the API 
2. Implement the operation that performs the Client check.

The Client check operation should check the document id validity (whether it's expired or not), 
query an external system for checking client's reputation 
(the client reputation is defined by a number: 
    0 to 20 means 'Candidate with no risk',
    21 to 99 means 'Candidate with medium risk, but enrollment still possible', 
    anything above 99 means 'Risky candidate, enrollment not acceptable')
and querying another external system to check whether the client already exists.

### Feel free to choose your favorite language, frameworks and tools :)    

  
